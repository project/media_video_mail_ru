<?php
/*
 * @file video_mail_ru.inc
 * Provides video.mail.ru integration for emvideo module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

/**
 *  This is the main URL for your provider.
 */
define('EMVIDEO_VIDEO_MAIL_RU_MAIN_URL', 'http://video.mail.ru/');

/**
 *  This defines the version of the content data array that we serialize
 *  in emvideo_video_mail_ru_data(). If we change the expected keys of that array,
 *  we must increment this value, which will allow older content to be updated
 *  to the new version automatically.
 */
define('EMVIDEO_VIDEO_MAIL_RU_DATA_VERSION', 1);

/**
 * hook emvideo_PROVIDER_info
 * This returns information relevant to a specific 3rd party video provider.
 *
 * @return
 *   A keyed array of strings requested by various admin and other forms.
 *    'provider' => The machine name of the provider. This must be the same as
 *      the base name of this filename, before the .inc extension.
 *    'name' => The translated name of the provider.
 *    'url' => The url to the main page for the provider.
 *    'settings_description' => A description of the provider that will be
 *      posted in the admin settings form.
 *    'supported_features' => An array of rows describing the state of certain
 *      supported features by the provider. These will be rendered in a table,
 *      with the columns being 'Feature', 'Supported', 'Notes'. In general,
 *      the 'Feature' column will give the name of the feature, 'Supported'
 *      will be Yes or No, and 'Notes' will give an optional description or
 *      caveats to the feature.
 */
function emvideo_video_mail_ru_info() {
  $features = array(
    array(t('RSS Attachment'), t('Yes'), ''),
    array(t('Thumbnails'), t('Yes'), t('')),
  );
  return array(
    'provider' => 'video_mail_ru',
    'name' => t('video.mail.ru'),
    'url' => EMVIDEO_VIDEO_MAIL_RU_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !video_mail_ru.', array('!video_mail_ru' => l(t('video.mail.ru'), EMVIDEO_VIDEO_MAIL_RU_MAIN_URL))),
    'supported_features' => $features,
  );
}

/**
 *  hook emvideo_PROVIDER_settings
 *  This should return a subform to be added to the emvideo_settings() admin
 *  settings page.
 *
 *  Note that a form field set will already be provided at $form['video_mail_ru'],
 *  so if you want specific provider settings within that field set, you should
 *  add the elements to that form array element.
 */
function emvideo_video_mail_ru_settings() {
  // We'll add a field set of player options here. You may add other options
  // to this element, or remove the field set entirely if there are no
  // user-configurable options allowed by the video_mail_ru provider.
  $form['video_mail_ru']['player_options'] = array(
    '#type' => 'markup',
    '#value' => t('There are no configurable options for video.mail.ru'),
  );

  return $form;
}

/**
 *  hook emvideo_PROVIDER_extract
 *
 *  This is called to extract the video code from a pasted URL or embed code.
 *
 *  We'll be passed a URL or the embed code from a video when an editor pastes
 *  that in the field's textfield. We'll need to either pass back an array of
 *  regex expressions to match, or do the matching ourselves and return the
 *  resulting video code.
 *
 *  @param $parse
 *    An optional string with the pasted URL or embed code.
 *  @return
 *    Either an array of regex expressions to be tested, or a string with the
 *    video code to be used. If the hook tests the code itself, it should
 *    return either the string of the video code (if matched), or an empty
 *    array. Otherwise, the calling function will handle testing the embed code
 *    against each regex string in the returned array.
 */
function emvideo_video_mail_ru_extract($parse = '') {
  // Here we assume that a URL will be passed in the form of
  // http://video.mail.ru/video/text-video-title
  // or embed code in the form of <object value="http://video.mail.ru/embed...".

  // We'll simply return an array of regular expressions for Embedded Media
  // Field to handle for us.
  /*
   our embed scripts are
   <object width=626 height=367>
   <param name="allowScriptAccess" value="always" />
   <param name="movie" value="http://img.mail.ru/r/video2/player_v2.swf?movieSrc=mail/krainov.vladimir/18/80" />
   <embed src=http://img.mail.ru/r/video2/player_v2.swf?movieSrc=mail/krainov.vladimir/18/80 type="application/x-shockwave-flash"
   width=626 height=367 allowScriptAccess="always"></embed></object>
   or our urls are
   http://video.mail.ru/mail/krainov.vladimir/18/80.html
   */
  return array(
    // In this expression, we're looking first for text matching the expression
    // between the @ signs. The 'i' at the end means we don't care about the
    // case.  We escape periods as \., as otherwise they match any character.
    // The text in parentheses () will be returned as the provider video code,
    // if there's a match for the entire expression. In this particular case,
    // ([^?]+) means to match one more more characters (+) that are not a
    // question mark ([^\?]), which would denote a query in the URL.
    '@video\.mail\.ru/(.+)\.html@i',

    // Now we test for embedded video code, which is similar in this case to
    // the above expression, except that we can safely assume we won't have a
    // query in the URL, and that the URL will be surrounded by quotation marks,
    // and have /embed/ rather than /video/ in the URL. Note that regular
    // expressions will be tested for matches in the order provided, so you
    // may need to move this value above the other in some cases. Obviously,
    // in the case of this video_mail_ru provider, you could easily improve the
    // regular expression to match against either a URL or embed code with
    // one expression, such as '@video\.mail\.ru/[watch|embed]/([^"\?]+)@i'.
    // However, many other providers have more complex requirements, so
    // we split them up for this demonstration.
    '@img\.mail\.ru/r/video2/player\_v2\.swf\?movieSrc=([^"]+)@i',
  );
}

/**
 *  hook emvideo_PROVIDER_data
 *
 *  Provides an array to be serialised and made available with $item elsewhere.
 *
 *  This data can be used to store any extraneous information available
 *  specifically to the video_mail_ru provider.
 */
function emvideo_video_mail_ru_data($field, $item) {
  // Initialize the data array.
  $data = array();

  //pics are
  //http://content.video.mail.ru/mail/krainov.vladimir/18/p-80.jpg

  // Create some version control. Thus if we make changes to the data array
  // down the road, we can respect older content. If allowed by Embedded Media
  // Field, any older content will automatically update this array as needed.
  // In any case, you should account for the version if you increment it.
  $data['emvideo_video_mail_ru_version'] = EMVIDEO_VIDEO_MAIL_RU_DATA_VERSION;
  
  $parts = explode('/', $item['value']);
  $last = array_pop($parts);
  $last = "p-$last.jpg";
  array_push($parts, $last);
  
  // This stores a URL to the video's thumbnail.
  $data['thumbnail'] = 'http://content.video.mail.ru/'. implode('/', $parts);
  return $data;
}

/**
 *  hook emvideo_PROVIDER_rss
 *
 *  This attaches a file to an RSS feed.
 */
function emvideo_video_mail_ru_rss($item, $teaser = NULL) {
  if ($item['value']) {
    $file['thumbnail']['filepath'] = $item['data']['thumbnail'];

    return $file;
  }
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video at the original provider's site.
 */
function emvideo_video_mail_ru_embedded_link($video_code) {
  return 'http://video.mail.ru/'. $video_code .'.html';
}

/**
 * The embedded flash displaying the video_mail_ru video.
 */
function theme_emvideo_video_mail_ru_flash($item, $width, $height, $autoplay) {
  $output = '';
  if ($item['embed']) {
    /*
     <object width=626 height=367>
   <param name="allowScriptAccess" value="always" />
   <param name="movie" value="http://img.mail.ru/r/video2/player_v2.swf?movieSrc=mail/krainov.vladimir/18/80" />
   <embed src=http://img.mail.ru/r/video2/player_v2.swf?movieSrc=mail/krainov.vladimir/18/80 type="application/x-shockwave-flash"
   width=626 height=367 allowScriptAccess="always"></embed></object>*/
    $autoplay = $autoplay ? 'true' : 'false';
    $output = '<object width='. $width .' height='. $height .'>
   <param name="allowScriptAccess" value="always" />
   <param name="movie" value="http://img.mail.ru/r/video2/player_v2.swf?movieSrc='. $item['value'] .'" />
   <embed src=http://img.mail.ru/r/video2/player_v2.swf?movieSrc='. $item['value'] .' type="application/x-shockwave-flash"
   width='. $width .' height='. $height .' allowScriptAccess="always"></embed></object>';
  }
  return $output;
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * Returns the external url for a thumbnail of a specific video.
 *  @param $field
 *    The field of the requesting node.
 *  @param $item
 *    The actual content of the field from the requesting node.
 *  @return
 *    A URL pointing to the thumbnail.
 */
function emvideo_video_mail_ru_thumbnail($field, $item, $formatter, $node, $width, $height) {
  // In this demonstration, we previously retrieved a thumbnail using oEmbed
  // during the data hook.
  return $item['data']['thumbnail'];
}

/**
 *  hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_video_mail_ru_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_video_mail_ru_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  hook emvideo_PROVIDER_video
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_video_mail_ru_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_video_mail_ru_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  Implementation of hook_emfield_subtheme.
 *  This returns any theme functions defined by this provider.
 */
function emvideo_video_mail_ru_emfield_subtheme() {
  $themes = array(
      'emvideo_video_mail_ru_flash'  => array(
          'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
          'file' => 'providers/video_mail_ru.inc',
          // If you don't provide a 'path' value, then it will default to
          // the emvideo.module path. Obviously, replace 'emvideo_mail_ru' with
          // the actual name of your custom module.
          'path' => drupal_get_path('module', 'media_video_mail_ru'),
      )
  );
  return $themes;
}
